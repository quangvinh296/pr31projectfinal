/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring_mvc_project_final.service;

import com.mycompany.spring_mvc_project_final.entities.ProductEntity;
import com.mycompany.spring_mvc_project_final.repository.ProductRepository;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author my
 */
@Service
public class ProductServiceImpl {
    
    @Autowired
    private ProductRepository productRepository;
    
    public List<ProductEntity> getProducts() {
        return (List<ProductEntity>) productRepository.findAll();
    }
    
    public void save(ProductEntity product) {
        productRepository.save(product);
    }
    
    public ProductEntity findProductById(int productId) {
        Optional<ProductEntity> product = productRepository.findById(productId);
       if (product.isPresent()) {
            return product.get();
        } else {
            return new ProductEntity();
        }
    }
   
     
    public boolean deleteProduct(int productId) {
        productRepository.deleteById(productId);
        return productRepository.existsById(productId);
    }
    
     public Set<ProductEntity> searchProduct(String strSearch) {
        return productRepository.findByNameContaining(strSearch);
    
    }
    
     public Set<ProductEntity> listProductOrderPrice(){
         return productRepository.findAllByOrderByPriceDesc();
     }
     
     public Set<ProductEntity> listProductOrderNew(){
         return productRepository.findAllByOrderByUpdateDateDesc();
     }
    
     public Set<ProductEntity> listProductOrderSelling(){
         return productRepository.findAllByOrderByQuantitySaleDesc();
     }
     
     public Set<ProductEntity> findListProductPriceByCategory(String nameCategory) {
         return productRepository.findByCategory_NameOrderByPriceDesc(nameCategory);
     }
     
      
     public Set<ProductEntity> findListProductNewByCategory(String nameCategory) {
         return productRepository.findByCategory_NameOrderByUpdateDateDesc(nameCategory);
     }
     
     public Set<ProductEntity> findListProductSellingByCategory(String nameCategory) {
         return productRepository.findByCategory_NameOrderByQuantitySaleDesc(nameCategory);
     }
     
     public Set<ProductEntity> findList8ProductNew(){
         return productRepository.findTop8ByOrderByUpdateDateDesc();
     }
}
