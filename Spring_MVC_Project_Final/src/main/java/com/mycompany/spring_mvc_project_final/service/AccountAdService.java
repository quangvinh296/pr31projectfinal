/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring_mvc_project_final.service;

import com.mycompany.spring_mvc_project_final.entities.CategoryEntity;
import com.mycompany.spring_mvc_project_final.entities.PersonInfo;
import com.mycompany.spring_mvc_project_final.entities.ProductEntity;
import com.mycompany.spring_mvc_project_final.entities.UserEntity;
import com.mycompany.spring_mvc_project_final.repository.AccountAdRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mycompany.spring_mvc_project_final.repository.CategoryAdRepository;
import com.mycompany.spring_mvc_project_final.repository.ProductRepository;
import java.util.Optional;
import java.util.Set;


@Service
public class AccountAdService {
    
    @Autowired
    private AccountAdRepository accountAdRepository;
    
     public List<UserEntity> getAccounts() {
        return (List<UserEntity>) accountAdRepository.findAll();
    }
     
     public void saveAll(List<UserEntity> accounts) {
        accountAdRepository.saveAll(accounts);
    }

    public void save(UserEntity account) {
        accountAdRepository.save(account);
    }
    
    public UserEntity findById(int id){
        Optional<UserEntity> account = accountAdRepository.findById(id);
        if(account.isPresent()){
            return account.get();
        }else{
            return new UserEntity();
        }
    }
    

}
