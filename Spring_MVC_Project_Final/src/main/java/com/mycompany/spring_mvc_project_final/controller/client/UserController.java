/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring_mvc_project_final.controller.client;

import com.mycompany.spring_mvc_project_final.entities.FavoriteEntity;
import com.mycompany.spring_mvc_project_final.entities.OrderEntity;
import com.mycompany.spring_mvc_project_final.entities.ProductEntity;
import com.mycompany.spring_mvc_project_final.entities.UserEntity;
import com.mycompany.spring_mvc_project_final.service.CategoryProImpl;
import com.mycompany.spring_mvc_project_final.service.FavoriteProServiceImpl;
import com.mycompany.spring_mvc_project_final.service.ImageServiceImpl;
import com.mycompany.spring_mvc_project_final.service.OrderDetailServiceImpl;
import com.mycompany.spring_mvc_project_final.service.OrderServiceImpl;
import com.mycompany.spring_mvc_project_final.service.ProductServiceImpl;
import com.mycompany.spring_mvc_project_final.service.UserServiceImpl;
import com.mycompany.spring_mvc_project_final.utils.UserUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private CategoryProImpl categoryService;

    @Autowired
    private ProductServiceImpl productService;

    @Autowired
    private FavoriteProServiceImpl favoriteService;

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private ImageServiceImpl imageService;

    @Autowired
    private OrderDetailServiceImpl orderDetailService;

    @RequestMapping("/home")
    public String viewHome(Model model, HttpServletRequest request, HttpSession session) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }
        UserEntity user = userService.findUserByEmail(username);
        model.addAttribute("user", user);
        model.addAttribute("categories", categoryService.getCategories());
        return "user/home";
    }

    @RequestMapping("/update")
    public String Update(Model model) {
        String username = UserUtils.User();
        UserEntity user = userService.findUserByEmail(username);
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("user", user);
        return "user/updateAccount";
    }

    @RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
    public String save(Model model, @ModelAttribute("user") UserEntity user) {
        UserEntity newUser = userService.findUserById(user.getId());
        newUser.setFullName(user.getFullName());
        newUser.setBirthDate(user.getBirthDate());
        newUser.setAddress(user.getAddress());
        newUser.setPhone(user.getPhone());
        userService.save(newUser);

        model.addAttribute("categories", categoryService.getCategories());
        return "user/home";
    }

    @RequestMapping("/history")
    public String historyOrder(Model model) {
        String username = UserUtils.User();
        UserEntity user = userService.findUserByEmail(username);

        model.addAttribute("orderDetail", orderDetailService.getOrderDetails());
        model.addAttribute("orders", orderService.findByUserId(user.getId()));
        model.addAttribute("categories", categoryService.getCategories());
        return "user/historyProduct";
    }

    @RequestMapping("/favorite/{id}")
    public String favoriteProduct(Model model,
            @PathVariable("id") int productId) {
        String username = UserUtils.User();
        UserEntity user = userService.findUserByEmail(username);
        ProductEntity product = productService.findProductById(productId);
        if (favoriteService.findByUserIdAndProductId(user.getId(), productId) == null) {
            FavoriteEntity favorite = new FavoriteEntity();
            favorite.setProduct(product);
            favorite.setUser(user);
            favoriteService.save(favorite);
        }

        model.addAttribute("favorite", "favorite");
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("message", "ByProduct");
        model.addAttribute("products", productService.listProductOrderNew());
        model.addAttribute("categories", categoryService.getCategories());
        return "listProduct";
    }

    @RequestMapping("/removeFavorite/{id}")
    public String removeFavorite(Model model,
            @PathVariable("id") int productId) {
        String username = UserUtils.User();
        UserEntity user = userService.findUserByEmail(username);
        favoriteService.deletefavoriteProduct(user.getId(), productId);
       
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("favorites", favoriteService.findProductByUserId(user.getId()));
        return "redirect:/user/favorite";
    }

    @RequestMapping("/favorite")
    public String favoriteList(Model model) {
        String username = UserUtils.User();
        UserEntity user = userService.findUserByEmail(username);
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("favorites", favoriteService.findProductByUserId(user.getId()));
        return "user/favoriteProduct";
    }
    

}
