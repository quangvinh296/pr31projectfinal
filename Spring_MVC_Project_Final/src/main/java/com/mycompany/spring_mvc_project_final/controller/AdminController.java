/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring_mvc_project_final.controller;

import com.mycompany.spring_mvc_project_final.entities.CategoryEntity;
import com.mycompany.spring_mvc_project_final.entities.ImageEntity;
import com.mycompany.spring_mvc_project_final.entities.ProductEntity;
import com.mycompany.spring_mvc_project_final.entities.PromotionEntity;
import com.mycompany.spring_mvc_project_final.entities.UserEntity;
import com.mycompany.spring_mvc_project_final.entities.UserRoleEntity;
import com.mycompany.spring_mvc_project_final.enums.ProductStatus;
import com.mycompany.spring_mvc_project_final.enums.PromotionStatus;
import com.mycompany.spring_mvc_project_final.service.AccountAdService;
import com.mycompany.spring_mvc_project_final.service.CategoryAdService;
import com.mycompany.spring_mvc_project_final.service.ImageServiceImpl;
import com.mycompany.spring_mvc_project_final.service.ProductAdService;
import com.mycompany.spring_mvc_project_final.service.PromotionAdService;
import com.mycompany.spring_mvc_project_final.service.UserRoleServiceImpl;
import com.mycompany.spring_mvc_project_final.service.UserServiceImpl;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AccountAdService accountService;

    @Autowired
    private UserServiceImpl userService;
    
    @Autowired
    private UserRoleServiceImpl roleService;
    
    @Autowired
    public JavaMailSender emailSender;
    
    @Autowired
    private CategoryAdService categoryService;

    @Autowired
    private ProductAdService productService;

    @Autowired
    private ImageServiceImpl imageService;

    @Autowired
    private PromotionAdService promotionService;

    @RequestMapping("/home")
    public String viewHome(Model model) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }

        model.addAttribute("message", "Hi " + username);
        return "admin/home3";
    }

    @RequestMapping(value = {"/listAdAccount"}, method = RequestMethod.GET)
    public String listAdAccount(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        model.addAttribute("accounts", accountService.getAccounts());
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/listAdAccount";
    }
    
    @RequestMapping("/add-account")
    public String addAccount(Model model,
            @RequestParam(name = "message", required = false) String message) {
        model.addAttribute("message", message);
        model.addAttribute("action", "add");
        return "admin/accountForm";
    }

    @RequestMapping("/addadmin")
    private String addAdmin(Model model, HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("user") UserEntity useInput, String siteURL) {
        UserEntity user = userService.findUserByEmail(useInput.getEmail());
        if (user != null) {
            model.addAttribute("message1", "Email registered");

        } else {
            user = userService.findUserLogin(useInput.getEmail(), true);
            if (user == null) {
                user = new UserEntity();
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                user.setPassword(encoder.encode(useInput.getPassword()));
                user.setFullName(useInput.getFullName());
                user.setAddress(useInput.getAddress());
                user.setPhone(useInput.getPhone());
                user.setBirthDate(useInput.getBirthDate());
                user.setEmail(useInput.getEmail());

                user.setGender(useInput.getGender());
                UserRoleEntity role = roleService.findRoleById(2);
                Set<UserRoleEntity> roles = new HashSet<>();
                roles.add(role);
                user.setUserRoles(roles);

                String randomCode = RandomString.make(64);
                user.setVerificationCode(randomCode);

                userService.save(user);
                SimpleMailMessage message = new SimpleMailMessage();

                message.setTo(useInput.getEmail());
                message.setSubject("Complete Registration!");
                message.setText("To verification your account, please click here : "
                        + "http://localhost:8080/Spring_MVC_Project_Final/verification?token=" + user.getVerificationCode());
                this.emailSender.send(message);
                model.addAttribute("message1", "Thank you for registration. Please check your Email to verification account !");
                model.addAttribute("categories", categoryService.getCategories());

//           
            }
        }
        return "admin/listAdAccount";
    }

    @RequestMapping("/addseller")
    private String addSeller(Model model, HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("user") UserEntity useInput, String siteURL) {
        UserEntity user = userService.findUserByEmail(useInput.getEmail());
        if (user != null) {
            model.addAttribute("message1", "Email registered");

        } else {
            user = userService.findUserLogin(useInput.getEmail(), true);
            if (user == null) {
                user = new UserEntity();
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                user.setPassword(encoder.encode(useInput.getPassword()));
                user.setFullName(useInput.getFullName());
                user.setAddress(useInput.getAddress());
                user.setPhone(useInput.getPhone());
                user.setBirthDate(useInput.getBirthDate());
                user.setEmail(useInput.getEmail());

                user.setGender(useInput.getGender());
                UserRoleEntity role = roleService.findRoleById(3);
                Set<UserRoleEntity> roles = new HashSet<>();
                roles.add(role);
                user.setUserRoles(roles);

                String randomCode = RandomString.make(64);
                user.setVerificationCode(randomCode);

                userService.save(user);
                SimpleMailMessage message = new SimpleMailMessage();

                message.setTo(useInput.getEmail());
                message.setSubject("Complete Registration!");
                message.setText("To verification your account, please click here : "
                        + "http://localhost:8080/Spring_MVC_Project_Final/verification?token=" + user.getVerificationCode());
                this.emailSender.send(message);
                model.addAttribute("message1", "Thank you for registration. Please check your Email to verification account !");
                model.addAttribute("categories", categoryService.getCategories());

//           
            }
        }
        return "admin/listAdAccount";
    }
    
    @RequestMapping(value = {"/listAdCategory"}, method = RequestMethod.GET)
    public String listAdCategory(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/listAdCategory";
    }

    @RequestMapping("/add-category")
    public String addCategory(Model model,
            @RequestParam(name = "message", required = false) String message) {
        model.addAttribute("category", new CategoryEntity());
        model.addAttribute("message", message);
        model.addAttribute("action", "add");
        return "admin/categoryForm";
    }

    @RequestMapping("/update-category/{categoryId}")
    public String updateCategory(Model model,
            @PathVariable("categoryId") int categoryId) {
        CategoryEntity category = categoryService.findById(categoryId);
        if (category.getId() >= 0) {
            model.addAttribute("category", category);
            model.addAttribute("action", "update");
            return "admin/categoryForm";
        } else {
            return "redirect:admin/home?type=error&message=Not found product id: " + categoryId;
        }
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String createCategory(Model model,
            HttpServletRequest request,
            @ModelAttribute("category") CategoryEntity category,
            @RequestParam("file") MultipartFile[] files) {
        categoryService.save(category);
        List<String> fileList = uploadFile(request, files);
        if (fileList != null) {
            for (String fileName : fileList) {
                if (!fileName.equalsIgnoreCase("")) {
                    ImageEntity image = new ImageEntity();
                    image.setName(fileName);
                    image.setCategory(category);
                    category.setNamePicture(fileName);
                    imageService.save(image);
                    categoryService.save(category);
                }
            }
        }
        return "redirect:/admin/listAdCategory";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateCategory(Model model,
            HttpServletRequest request,
            @ModelAttribute("category") CategoryEntity category,
            @RequestParam("file") MultipartFile[] files) {
        categoryService.save(category);
        List<String> fileList = uploadFile(request, files);
        if (fileList != null) {
            for (String fileName : fileList) {
                if (!fileName.equalsIgnoreCase("")) {
                    ImageEntity image = new ImageEntity();
                    image.setName(fileName);
                    image.setCategory(category);
                    category.setNamePicture(fileName);
                    imageService.save(image);
                    categoryService.save(category);
                }
            }
        }
        return "redirect:/admin/listAdCategory";
    }

    @RequestMapping(value = {"/listAdProduct"}, method = RequestMethod.GET)
    public String listAdProduct(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        model.addAttribute("products", productService.getProducts());
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/listAdProduct";
    }

    @RequestMapping("/add-product")
    public String addProduct(Model model) {
        model.addAttribute("product", new ProductEntity());
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("status", ProductStatus.values());
        model.addAttribute("action", "addproduct");
        return "admin/productForm";
    }

    @RequestMapping(value = "/addproduct", method = RequestMethod.POST)
    public String createProduct(Model model,
            HttpServletRequest request,
            @ModelAttribute("product") ProductEntity product,
            @RequestParam("file") MultipartFile[] files) {
        productService.save(product);
        List<String> fileList = uploadFileProduct(request, files);
        if (fileList != null) {
            for (String fileName : fileList) {
                if (!fileName.equalsIgnoreCase("")) {
                    ImageEntity image = new ImageEntity();
                    image.setName(fileName);
                    image.setProduct(product);
                    imageService.save(image);
                }
            }
        }
        return "redirect:/admin/listAdProduct";
    }

    @RequestMapping("/update-product/{productId}")
    public String updateProduct(Model model,
            @PathVariable("productId") int productId) {
        ProductEntity product = productService.findById(productId);
        if (product.getId() >= 0) {
            model.addAttribute("product", product);
            model.addAttribute("categories", categoryService.getCategories());
            model.addAttribute("status", ProductStatus.values());
            model.addAttribute("action", "updateproduct");
            return "admin/productForm";
        } else {
            return "redirect:admin/home?type=error&message=Not found product id: " + productId;
        }
    }

    @RequestMapping(value = "/updateproduct", method = RequestMethod.POST)
    public String updateProduct(Model model,
            HttpServletRequest request,
            @ModelAttribute("product") ProductEntity product,
            @RequestParam("file") MultipartFile[] files) {
        productService.save(product);
        List<String> fileList = uploadFileProduct(request, files);
        if (fileList != null) {
            for (String fileName : fileList) {
                if (!fileName.equalsIgnoreCase("")) {
                    ImageEntity image = new ImageEntity();
                    image.setName(fileName);
                    image.setProduct(product);
                    imageService.save(image);
                }
            }
        }
        return "redirect:/admin/listAdProduct";
    }

    @RequestMapping(value = {"/listAdPromotion"}, method = RequestMethod.GET)
    public String listAdPromotion(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {

        model.addAttribute("promotions", promotionService.getPromotions());
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "admin/listAdPromotion";
    }

    @RequestMapping("/add-promotion")
    public String addPromotion(Model model) {
        model.addAttribute("promotion", new PromotionEntity());
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("status", PromotionStatus.values());
        model.addAttribute("action", "addpromotion");
        return "admin/promotionForm";
    }

    @RequestMapping(value = "/addpromotion", method = RequestMethod.POST)
    public String createPromotion(Model model,
            HttpServletRequest request,
            @ModelAttribute("promotion") PromotionEntity promotion) {
        promotionService.save(promotion);
        return "redirect:/admin/listAdPromotion";
    }

    @RequestMapping("/update-promotion/{promotionId}")
    public String updatePromotion(Model model,
            @PathVariable("promotionId") int promotionId) {
        PromotionEntity promotion = promotionService.findPromotionById(promotionId);
        if (promotion.getId() >= 0) {
            model.addAttribute("promotion", promotion);
            model.addAttribute("status", PromotionStatus.values());
            model.addAttribute("action", "updatepromotion");
            return "admin/promotionForm";
        } else {
            return "redirect:admin/home?type=error&message=Not found product id: " + promotionId;
        }
    }

    @RequestMapping(value = "/updatepromotion", method = RequestMethod.POST)
    public String updatePromotion(Model model,
            HttpServletRequest request,
            @ModelAttribute("promotion") PromotionEntity promotion) {
        promotionService.save(promotion);
        return "redirect:/admin/listAdPromotion";
    }

    public List<String> uploadFile(HttpServletRequest request, @RequestParam("file") MultipartFile[] files) {
        List<String> fileList = new ArrayList<>();
        if (files != null && files.length > 0) {

            for (int i = 0; i < files.length; i++) {
                MultipartFile file = files[i];
                try {
                    byte[] bytes = file.getBytes();

                    ServletContext context = request.getServletContext();
                    String pathUrl = context.getRealPath("/images");

                    int index = pathUrl.indexOf("target");
                    String pathFolder = pathUrl.substring(0, index) + "src\\main\\webapp\\resources\\img\\logo\\";
                    Path path = Paths.get(pathFolder + file.getOriginalFilename());
                    Files.write(path, bytes);

                    // sau khi upload file xong lấy file name ra để insert vào database
                    String name = file.getOriginalFilename();
                    fileList.add(name);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

        }
        return fileList;

    }

    public List<String> uploadFileProduct(HttpServletRequest request, @RequestParam("file") MultipartFile[] files) {
        List<String> fileList = new ArrayList<>();
        if (files != null && files.length > 0) {

            for (int i = 0; i < files.length; i++) {
                MultipartFile file = files[i];
                try {
                    byte[] bytes = file.getBytes();

                    ServletContext context = request.getServletContext();
                    String pathUrl = context.getRealPath("/images");

                    int index = pathUrl.indexOf("target");
                    String pathFolder = pathUrl.substring(0, index) + "src\\main\\webapp\\resources\\img\\category\\";
                    Path path = Paths.get(pathFolder + file.getOriginalFilename());
                    Files.write(path, bytes);

                    // sau khi upload file xong lấy file name ra để insert vào database
                    String name = file.getOriginalFilename();
                    fileList.add(name);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

        }
        return fileList;

    }
}
