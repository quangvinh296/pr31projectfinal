/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring_mvc_project_final.controller.client;

import com.mycompany.spring_mvc_project_final.entities.CreditCardEntity;
import com.mycompany.spring_mvc_project_final.entities.OrderDetailEntity;
import com.mycompany.spring_mvc_project_final.entities.OrderEntity;
import com.mycompany.spring_mvc_project_final.entities.PaymentEntity;
import com.mycompany.spring_mvc_project_final.entities.ProductEntity;
import com.mycompany.spring_mvc_project_final.entities.UserEntity;
import com.mycompany.spring_mvc_project_final.enums.OrderStatus;
import com.mycompany.spring_mvc_project_final.enums.PaymentStatus;
import com.mycompany.spring_mvc_project_final.enums.ProductStatus;
import com.mycompany.spring_mvc_project_final.service.CategoryProImpl;
import com.mycompany.spring_mvc_project_final.service.CreditCardServiceImpl;
import com.mycompany.spring_mvc_project_final.service.ImageServiceImpl;
import com.mycompany.spring_mvc_project_final.service.OrderDetailServiceImpl;
import com.mycompany.spring_mvc_project_final.service.OrderServiceImpl;
import com.mycompany.spring_mvc_project_final.service.PaymentServiceImpl;
import com.mycompany.spring_mvc_project_final.service.ProductServiceImpl;
import com.mycompany.spring_mvc_project_final.service.UserServiceImpl;
import com.mycompany.spring_mvc_project_final.utils.UserUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author my
 */
@Controller
public class CartController {
    
    @Autowired
    private CategoryProImpl categoryService;
    
    @Autowired
    private OrderServiceImpl orderService;
    
    @Autowired
    private ProductServiceImpl productService;
    
    @Autowired
    public PaymentServiceImpl paymentService;
    
    @Autowired
    public JavaMailSender emailSender;
    
    @Autowired
    private CreditCardServiceImpl cardService;
    
    @Autowired
    private UserServiceImpl userService;
    
    @Autowired
    private ImageServiceImpl imageService;
    
    @Autowired
    private OrderDetailServiceImpl orderDetailService;
    
    @RequestMapping(value = "/update/{productId}", method = RequestMethod.POST)
    public String orderProduct(Model model, HttpServletRequest request,
            HttpSession session, @RequestParam(name = "button", required = false) String button,
            @RequestParam(name = "quantity_input", required = false) String quantity_value,
            @PathVariable("productId") int productId) {
        
        HashMap<Integer, OrderDetailEntity> orderDetails = (HashMap<Integer, OrderDetailEntity>) session.getAttribute("myCart");
        if (orderDetails == null) {
            orderDetails = new HashMap<>();
        }
        int quantity = Integer.parseInt(quantity_value);
        ProductEntity product = productService.findProductById(productId);
        if (orderDetails.containsKey(productId)) {
            
            OrderDetailEntity item = orderDetails.get(productId);
            
            if (button.equalsIgnoreCase("-")) {
                if (quantity == 1) {
                    orderDetails.remove(productId);
                } else {
                    item.setProduct(product);
                    item.setQuantity(quantity - 1);
                    orderDetails.put(productId, item);
                }
            }
            if (button.equalsIgnoreCase("+")) {
                if (quantity == product.getQuantityProduct()) {
                    model.addAttribute("error", "Product " + product.getName() + " have " + product.getQuantityProduct() + " items");
                } else {
                    item.setProduct(product);
                    item.setQuantity(quantity + 1);
                    orderDetails.put(productId, item);
                }
            }
        }
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("productDetail", productService.findProductById(productId));
        session.setAttribute("myCart", orderDetails);
        session.setAttribute("myCartTotal", totalPrice(orderDetails));
        session.setAttribute("myCartTotalQuantity", totalQuantityProduct(orderDetails));
        return "cart";
        
    }
    
    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String orderProduct(Model model, HttpServletRequest request,
            HttpSession session) {
        HashMap<Integer, OrderDetailEntity> orderDetails = (HashMap<Integer, OrderDetailEntity>) session.getAttribute("myCart");
        if (orderDetails == null) {
            orderDetails = new HashMap<>();
        }
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("productDetail", new OrderDetailEntity());
        session.setAttribute("myCart", orderDetails);
        session.setAttribute("myCartTotal", totalPrice(orderDetails));
        session.setAttribute("myCartTotalQuantity", totalQuantityProduct(orderDetails));
        return "cart";
        
    }
    
    @RequestMapping(value = "remove/{Id}", method = RequestMethod.GET)
    public String viewRemove(Model model, HttpServletRequest request,
            HttpSession session,
            @PathVariable("Id") int id) {
        HashMap<Integer, OrderDetailEntity> orderDetails = (HashMap<Integer, OrderDetailEntity>) session.getAttribute("myCart");
        if (orderDetails == null) {
            orderDetails = new HashMap<>();
            
        }
        model.addAttribute("images", imageService.getImagesDistinct());
        orderDetails.remove(id);
        model.addAttribute("categories", categoryService.getCategories());
        session.setAttribute("myCart", orderDetails);
        session.setAttribute("myCartTotal", totalPrice(orderDetails));
        session.setAttribute("myCartTotalQuantity", totalQuantityProduct(orderDetails));
        return "cart";
        
    }
    
    @RequestMapping("/updateQuantity/{productId}")
    public String updateQuantity(Model model, HttpServletRequest request,
            HttpSession session,
            @RequestParam(name = "quantity_input", required = false) String quantity_value,
            @PathVariable("productId") int productId) {
        HashMap<Integer, OrderDetailEntity> orderDetails = (HashMap<Integer, OrderDetailEntity>) session.getAttribute("myCart");
        if (orderDetails == null) {
            orderDetails = new HashMap<>();
        }
        int quantity = Integer.parseInt(quantity_value);
        ProductEntity product = productService.findProductById(productId);
        if (product != null) {
            if (product.getQuantityProduct() >= quantity) {
                if (orderDetails.containsKey(productId)) {
                    OrderDetailEntity item = orderDetails.get(productId);
                    item.setProduct(product);
                    item.setQuantity(item.getQuantity() + quantity);
                    orderDetails.put(productId, item);
                } else {
                    OrderDetailEntity item = new OrderDetailEntity();
                    item.setProduct(product);
                    item.setQuantity(quantity);
                    orderDetails.put(productId, item);
                }
            } else {
                model.addAttribute("error", "Product have " + product.getQuantityProduct() + " item");
            }
        }
        model.addAttribute("message1", "description");
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("productDetail", productService.findProductById(productId));
        session.setAttribute("myCart", orderDetails);
        session.setAttribute("myCartTotal", totalPrice(orderDetails));
        session.setAttribute("myCartTotalQuantity", totalQuantityProduct(orderDetails));
        return "productDetail";
    }
    
    @RequestMapping("/checkout")
    public String Checkout(Model model, HttpServletRequest request,
            @RequestParam("totalBill") double totalBill,
            @RequestParam(name = "discount", required = false) double discount,
            HttpSession session) {
        HashMap<Integer, OrderDetailEntity> orderDetails = (HashMap<Integer, OrderDetailEntity>) session.getAttribute("myCart");
        if (orderDetails == null) {
            orderDetails = new HashMap<>();
            return "redirect:/home";
        } else {
            session.setAttribute("myCart", orderDetails);
            session.setAttribute("myCartTotal", totalPrice(orderDetails));
            session.setAttribute("myCartTotalQuantity", totalQuantityProduct(orderDetails));
            model.addAttribute("categories", categoryService.getCategories());
            model.addAttribute("totalBill", totalBill);
            model.addAttribute("discount", discount);
            String username = UserUtils.User();
            UserEntity user = userService.findUserByEmail(username);
            if (user != null) {
                model.addAttribute("user", user);
            }
            return "checkout";
        }
    }
    
    @RequestMapping(value = "/creditcard", method = RequestMethod.POST)
    private String addCart(@ModelAttribute("user") UserEntity userForm, Model model,
            @RequestParam("totalBill") double totalBill,
            HttpServletRequest request, HttpSession session) {
        OrderEntity orders = new OrderEntity();
        String username = UserUtils.User();
        UserEntity user = userService.findUserByEmail(username);
        if (user == null) {
            orders.setPhone(userForm.getPhone());
            orders.setFullName(userForm.getFullName());
            orders.setAddress(userForm.getAddress());
            orders.setEmail(userForm.getEmail());
        } else {
            orders.setUser(user);
        }
        
        orders.setTotalOrder(totalBill);
        orders.setDateOrder(new Date());
        
        request.getSession().setAttribute("myorder", orders);
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("card", new CreditCardEntity());
        return "creditCard";
    }
    
    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    public String finishCart(Model model, @ModelAttribute("card") CreditCardEntity getCard,
            @RequestParam("expiry") String expiry,
            @RequestParam("totalBill") double totalBill,
            HttpServletRequest request, HttpSession session) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        OrderEntity order = (OrderEntity) session.getAttribute("myorder");
        CreditCardEntity card = cardService.getCreditCard(getCard.getCardNumber());
        
        if (card == null) {
            model.addAttribute("error", "CreditCard not exists !!!");
            model.addAttribute("categories", categoryService.getCategories());
            model.addAttribute("card", new CreditCardEntity());
            model.addAttribute("totalBill", totalBill);
            return "creditCard";
        } else {
            String[] expiryParts = expiry.split("/");
            int exipyMonth = Integer.parseInt(expiryParts[0].trim());
            int expiryYear = Integer.parseInt(expiryParts[1].trim());
            if (card.getCvvcode() != getCard.getCvvcode() || !card.getOwnerName().equals(getCard.getOwnerName())
                    || card.getExpiryMonth() != exipyMonth || card.getExpiryYear() != expiryYear) {
                model.addAttribute("error", "Wrong CreditCard information !!!");
                model.addAttribute("card", new CreditCardEntity());
                model.addAttribute("totalBill", totalBill);
                model.addAttribute("categories", categoryService.getCategories());
                return "creditCard";
            } else {
                if (card.getBalance() < totalBill) {
                    model.addAttribute("error", "Balances not enough money !!!");
                    model.addAttribute("card", new CreditCardEntity());
                    model.addAttribute("totalBill", totalBill);
                    return "creditCard";
                } else {
                    order.setOrderUId(generateUID());
                    order.setStatus(OrderStatus.COMPLETED);
                    
                    orderService.save(order);
                    
                    HashMap<Integer, OrderDetailEntity> orderDetails = (HashMap<Integer, OrderDetailEntity>) session.getAttribute("myCart");
                    Collection<OrderDetailEntity> values = orderDetails.values();
                    ArrayList<OrderDetailEntity> listOrderDetails = new ArrayList<OrderDetailEntity>(values);
                    OrderDetailEntity orderDetail = new OrderDetailEntity();
                    for (Map.Entry<Integer, OrderDetailEntity> list : orderDetails.entrySet()) {
                        ProductEntity product = productService.findProductById(list.getValue().getProduct().getId());
                        int quantity = list.getValue().getProduct().getQuantityProduct() - list.getValue().getQuantity();
                        if (quantity > 0) {
                            product.setStatus(ProductStatus.IN_STOCK);
                            product.setQuantityProduct(quantity);
                        } else {
                            product.setStatus(ProductStatus.OUT_OF_STOCK);
                            product.setQuantityProduct(0);
                        }
                        productService.save(product);
                    }
                    PaymentEntity payment = new PaymentEntity();
                    payment.setOrder(order);
                    payment.setAmount(totalBill);
                    payment.setPaymentDate(new Date());
                    payment.setCreditcard(card);
                    payment.setStatus(PaymentStatus.PAYMENT_ORDER);
                    paymentService.save(payment);
                    
                    card.setBalance(card.getBalance() - totalBill);
                    cardService.saveCard(card);
                    
                    orderDetail.setOrder(order);
                    orderDetail.setQuantity((int) totalQuantityProduct(orderDetails));
                    orderDetailService.save(orderDetail);

                    SimpleMailMessage message = new SimpleMailMessage();

                    message.setTo(order.getEmail());
                    message.setSubject("Complete Registration!");
                    String mailContent = "<p>Dear " + order.getFullName() + ",</p>";
                    mailContent += "<p>Number Order " + order.getOrderUId() + ",</p>";
                    mailContent += "<p>Date order: " + order.getDateOrder() + ",</p>";
                    mailContent += "<p>Your Email: " + order.getEmail() + ",</p>";
                    mailContent += "<p>Your Phone: " + order.getPhone() + ",</p>";
                    mailContent += "<p>Your Address: " + order.getAddress() + ",</p>";
                    mailContent += "if there is an error please call hotel phone 035694987";
                    mailContent += "<p>Thank you<br>Sublime OnlineShop</p>";

                    message.setText(mailContent);
                    this.emailSender.send(message);
                    model.addAttribute("categories", categoryService.getCategories());
                    request.getSession().removeAttribute("myCart");
                    request.getSession().removeAttribute("myorder");
                    model.addAttribute("categories", categoryService.getCategories());
                    model.addAttribute("orderUID", order.getOrderUId());
                    
                    return "finishCart";
                }
            }
        }
    }
    
    public double totalPrice(HashMap<Integer, OrderDetailEntity> orderDetails) {
        int count = 0;
        for (Map.Entry<Integer, OrderDetailEntity> list : orderDetails.entrySet()) {
            count += list.getValue().getProduct().getPrice() * list.getValue().getQuantity();
        }
        return count;
    }
    
    public double totalQuantityProduct(HashMap<Integer, OrderDetailEntity> orderDetails) {
        int sumquantity = 0;
        for (Map.Entry<Integer, OrderDetailEntity> list : orderDetails.entrySet()) {
            sumquantity += list.getValue().getQuantity();
        }
        return sumquantity;
    }
    
    private String generateUID() {
        String maxId = orderService.getMaxOrderId();
        int id;
        if (maxId == null) {
            id = 1;
        } else {
            id = Integer.parseInt(maxId);
        }
        Random rnd = new Random();
        int n = 1000 + rnd.nextInt(9000);
        int m = 1000 + rnd.nextInt(9000);
        String uid = n + "-" + m + (String.format("%05d", id + 1));
        return uid;
    }
}
