/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring_mvc_project_final.controller.client;

import com.mycompany.spring_mvc_project_final.entities.CreditCardEntity;
import com.mycompany.spring_mvc_project_final.entities.OrderEntity;
import com.mycompany.spring_mvc_project_final.entities.PaymentEntity;
import com.mycompany.spring_mvc_project_final.entities.ProductEntity;
import com.mycompany.spring_mvc_project_final.entities.PromotionEntity;
import com.mycompany.spring_mvc_project_final.entities.UserEntity;
import com.mycompany.spring_mvc_project_final.entities.UserRoleEntity;
import com.mycompany.spring_mvc_project_final.enums.Gender;
import com.mycompany.spring_mvc_project_final.enums.UserStatus;
import com.mycompany.spring_mvc_project_final.service.CategoryProImpl;
import com.mycompany.spring_mvc_project_final.service.CreditCardServiceImpl;
import com.mycompany.spring_mvc_project_final.service.ImageServiceImpl;
import com.mycompany.spring_mvc_project_final.service.OrderDetailServiceImpl;
import com.mycompany.spring_mvc_project_final.service.OrderServiceImpl;
import com.mycompany.spring_mvc_project_final.service.PaymentServiceImpl;
import com.mycompany.spring_mvc_project_final.service.ProductServiceImpl;
import com.mycompany.spring_mvc_project_final.service.PromotionServiceImpl;
import com.mycompany.spring_mvc_project_final.service.UserRoleServiceImpl;
import com.mycompany.spring_mvc_project_final.service.UserServiceImpl;
import com.mycompany.spring_mvc_project_final.utils.SecurityUtils;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private ProductServiceImpl productService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private CategoryProImpl categoryService;

    @Autowired
    private ImageServiceImpl imageService;

    @Autowired
    private UserRoleServiceImpl roleService;

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    public PromotionServiceImpl promotionService;

    @Autowired
    private OrderDetailServiceImpl orderDetailService;

    @Autowired
    private PaymentServiceImpl paymentService;

    @Autowired
    private CreditCardServiceImpl cardService;

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String welcomePage(Model model) {
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("products", productService.findList8ProductNew());

        List<String> roles = SecurityUtils.getRolesOfUser();
        if (!CollectionUtils.isEmpty(roles) && (roles.contains("ROLE_ADMIN")
                || roles.contains("ROLE_SELLER") || roles.contains("ROLE_MANAGER"))) {
            return "redirect:/admin/home";
        }

        return "home2";
    }

    @RequestMapping("/403")
    public String accessDenied(Model model) {
        model.addAttribute("categories", categoryService.getCategories());
        return "403";
    }

    @RequestMapping("/aboutUs")
    public String AboutUs(Model model,
            @RequestParam(name = "message", required = false) String message) {
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("message", message);
        return "aboutUs";
    }

    @RequestMapping(value = "/findOrder", method = RequestMethod.GET)
    public String findOrderPage(Model model) {
        return "redirect:/aboutUs?message=find";
    }

    @RequestMapping(value = "/findOrder", method = RequestMethod.POST)
    public String findOrder(Model model,
            @RequestParam(name = "orderSearch", required = false) String orderSearch) {
        Set<OrderEntity> orders = orderService.findByOrderUid(orderSearch);
        if (orders == null) {
            model.addAttribute("type", "notFound");
        }
        model.addAttribute("message", "findOrder");
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("orders", orders);
        model.addAttribute("orderDetail", orderDetailService.getOrderDetails());
        return "aboutUs";
    }
// list all of Category
    @RequestMapping("/listCategory")
    public String ListCategory(Model model) {
        model.addAttribute("categories", categoryService.getCategories());
        return "listCategory";
    }
// list all of products
    @RequestMapping(value = "/listProduct", method = RequestMethod.GET)
    public String ListAllProduct(Model model) {
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("message", "ByProduct");
        model.addAttribute("products", productService.listProductOrderNew());
        model.addAttribute("categories", categoryService.getCategories());

        return "listProduct";
    }
     @RequestMapping(value = "/{byorder}", method = RequestMethod.GET)
    public String ListAllProductBy(Model model, @PathVariable("byorder") String byorder) {
        if (byorder.equalsIgnoreCase("new")) {
            model.addAttribute("products", productService.listProductOrderNew());
        }
        if (byorder.equalsIgnoreCase("price")) {
            model.addAttribute("products", productService.listProductOrderPrice());
        }
        if (byorder.equalsIgnoreCase("selling")) {
            model.addAttribute("products", productService.listProductOrderSelling());
        }
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("message", "ByProduct");
        model.addAttribute("categories", categoryService.getCategories());
        return "listProduct";
    }
    
    // list all of product by category
      @RequestMapping(value = "/listProduct/{nameCategory}", method = RequestMethod.GET)
    public String ListByCategory(Model model,
            @PathVariable("nameCategory") String name) {
        model.addAttribute("products", productService.findListProductNewByCategory(name));
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("message", "byCategory");
        model.addAttribute("category", name);
        model.addAttribute("categories", categoryService.getCategories());
        return "listProduct";
    }
    
     @RequestMapping(value = "/{nameCategory}/{byorder}", method = RequestMethod.GET)
    public String ListByCategory(Model model,
            @PathVariable("nameCategory") String name, @PathVariable("byorder") String byorder) {
        if (byorder.equalsIgnoreCase("new")) {
            model.addAttribute("products", productService.findListProductNewByCategory(name));
        }
        if (byorder.equalsIgnoreCase("price")) {
            model.addAttribute("products", productService.findListProductPriceByCategory(name));
        }
        if (byorder.equalsIgnoreCase("selling")) {
            model.addAttribute("products", productService.findListProductSellingByCategory(name));
        }
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("message", "byCategory");
        model.addAttribute("category", name);
        model.addAttribute("categories", categoryService.getCategories());
        return "listProduct";
    }

   

    @RequestMapping("/searchProduct")
    public String searchProduct(Model model,
            @ModelAttribute("strSearch") String strSearch) {
        model.addAttribute("images", imageService.getImagesDistinct());
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("products", productService.searchProduct(strSearch));
        return "listProduct";
    }

    @RequestMapping("/productDetail/{idProduct}")
    public String ProductDetail(Model model, @PathVariable("idProduct") int id) {
        ProductEntity product = productService.findProductById(id);

        if (product.getQuantityProduct() == 0) {
            model.addAttribute("message", "Out of Stock");
        }
        model.addAttribute("img", imageService.getImagesDistinct());
        model.addAttribute("images", imageService.getImages());
        model.addAttribute("message1", "description");
        model.addAttribute("productDetail", productService.findProductById(id));
        model.addAttribute("categories", categoryService.getCategories());
        return "productDetail";
    }

    @RequestMapping("/technical/{idProduct}")
    public String detailTechnical(Model model,
            @PathVariable("idProduct") int id) {
        model.addAttribute("message1", "technical");
        model.addAttribute("productDetail", productService.findProductById(id));
        model.addAttribute("categories", categoryService.getCategories());
        return "productDetail";
    }

    @RequestMapping("/description/{idProduct}")
    public String description(Model model,
            @PathVariable("idProduct") int id) {
        model.addAttribute("message1", "description");
        model.addAttribute("productDetail", productService.findProductById(id));
        model.addAttribute("categories", categoryService.getCategories());
        return "productDetail";
    }

    @RequestMapping("/login")
    public String loginPage(Model model, @RequestParam(value = "error", required = false) boolean error) {
        if (error) {
            model.addAttribute("message", "Login Fail!!!");
        }
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("genders", Gender.values());
        model.addAttribute("user", new UserEntity());
        return "login";
    }

    @RequestMapping("/register")
    public String registerPage(Model model) {

        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("genders", Gender.values());
        model.addAttribute("user", new UserEntity());
        return "register";
    }

    @RequestMapping(value = {"/Register"}, method = RequestMethod.POST)
    public String register(Model model, HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("user") UserEntity useInput) {
        UserEntity user = userService.findUserLogin(useInput.getEmail(), "UN_ACTIVE");
        if (user != null) {
            model.addAttribute("message1", "Email registered");

        } else {
            user = new UserEntity();
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            user.setPassword(encoder.encode(useInput.getPassword()));
            user.setFullName(useInput.getFullName());
            user.setAddress(useInput.getAddress());
            user.setPhone(useInput.getPhone());
            user.setBirthDate(useInput.getBirthDate());
            user.setEmail(useInput.getEmail());
            user.setStatus(UserStatus.UN_ACTIVE);
            user.setGender(useInput.getGender());
            UserRoleEntity role = roleService.findRoleById(1);
            Set<UserRoleEntity> roles = new HashSet<>();
            roles.add(role);
            user.setUserRoles(roles);

            String randomCode = RandomString.make(64);
            user.setVerificationCode(randomCode);

            userService.save(user);
            SimpleMailMessage message = new SimpleMailMessage();

            message.setTo(useInput.getEmail());
            message.setSubject("Complete Registration!");
            message.setText("To verification your account, please click here : "
                    + "http://localhost:8080/Spring_MVC_Project_Final/verification?token=" + user.getVerificationCode());
            this.emailSender.send(message);
            model.addAttribute("message1", "Thank you for registration. Please check your Email to verification account !");
            model.addAttribute("categories", categoryService.getCategories());

        }
        return "register";
    }

    @RequestMapping("/verification")
    private String verification(@RequestParam("token") String token, Model model, HttpSession session) {

        if (token == null) {
            model.addAttribute("msg", "The link is invalid or broken !");
        } else {
            UserEntity user = (UserEntity) session.getAttribute("user");
            user.setStatus(UserStatus.ACTIVE);
            user.setVerificationCode(null);
            userService.save(user);
        }
        model.addAttribute("categories", categoryService.getCategories());
        return "register";
    }

    @RequestMapping("/promotion")
    public String addPromo(Model model,
            @RequestParam(name = "coupon_input", required = false) String strPromo) {
        model.addAttribute("categories", categoryService.getCategories());
        PromotionEntity pro = promotionService.findPromotionByCode(strPromo);
        if (pro.isEnabled()) {
            model.addAttribute("pro", "success");
            model.addAttribute("discount", pro.getDiscount());
        } else {
            model.addAttribute("pro", "error");
        }
        return "cart";
    }

    @RequestMapping("/cancelOrder/{orderId}")
    public String cancelOrder(Model model, @PathVariable("orderId") int orderId) {
        OrderEntity order = orderService.findOrderById(orderId);
        List<PaymentEntity> pays = paymentService.getPaysByOrderID(orderId);

        if (order.getId() > 0) {
            if (!orderService.deleteOrder(orderId)) {
                for (PaymentEntity pay : pays) {
                    double balanceNew = pay.getCreditcard().getBalance() + pay.getAmount();
                    CreditCardEntity card = cardService.findCardById(pay.getCreditcard().getCardId());
                    card.setBalance(balanceNew);
                    cardService.saveCard(card);
                    paymentService.deletePayment(pay.getId());
                }
//                return "redirect:/home?type=success&message=Delete book id: " + bookId + " success";
            } else {
//                return "redirect:/home?type=error&message=Delete book id: " + bookId + " fail";
            }
        } else {
//            return "redirect:/home?type=error&message=Not found book id: " + bookId;
        }

        model.addAttribute("categories", categoryService.getCategories());
        return "redirect:/findOrder";
    }

// các chương trình khuyến mãi, tạo country, city trong form register,phan trang
// rà soát lại các đoạn code, hình ảnh sp trong productDetail, remember me, tại lại mật khẩu tài khoản
    // cancel order đã đặt,tạo comment
}
