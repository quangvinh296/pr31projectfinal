/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring_mvc_project_final.repository;

import com.mycompany.spring_mvc_project_final.entities.ProductEntity;

import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author my
 */
@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {
   
    Set<ProductEntity> findByCategory_NameOrderByUpdateDateDesc(String nameCategory);

    Set<ProductEntity> findByCategory_NameOrderByPriceDesc(String nameCategory);
    
    Set<ProductEntity> findByCategory_NameOrderByQuantitySaleDesc(String nameCategory);
    
    Set<ProductEntity> findAllByOrderByPriceDesc();
    
    Set<ProductEntity> findAllByOrderByUpdateDateDesc();
    
    Set<ProductEntity> findAllByOrderByQuantitySaleDesc();
    
    Set<ProductEntity> findTop8ByOrderByUpdateDateDesc();
    
    Set<ProductEntity> findByNameContaining(String name);
}
