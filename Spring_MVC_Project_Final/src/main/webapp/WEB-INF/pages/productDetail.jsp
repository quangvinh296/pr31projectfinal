<%-- 
    Document   : productDetail
    Created on : Nov 23, 2020, 11:23:39 AM
    Author     : my
--%>
<%@taglib  uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="include/css-header.jsp" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/product.css"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/product_responsive.css"/>"/>

    </head>
    <body>
        <div class="super_container">
            <jsp:include page="include/menu1.jsp" />
            <!-- Home -->

            <div class="home">
                <div class="home_container">
                    <div class="home_background" style="background-image:url(${pageContext.request.contextPath}/resources/images/categories.jpg)"></div>
                    <div class="home_content_container">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div class="home_content">
                                        <div class="home_title">${productDetail.name}<span>.</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Product Details -->

            <div class="product_details">
                <div class="container">
                    <div class="row details_row">

                        <!-- Product Image -->
                        <div class="col-lg-6">
                            <div class="details_image">
                                <c:forEach items="${images}" var="image" >
                                    <c:if test="${p.id == image.product.id}">
                                        <div class="product_image"  style="height:300px;width:250px;"><a  href="${pageContext.request.contextPath}/productDetail/${p.id}"><img src="<c:url value="/resources/img/category/${image.name}"/>" alt=""></a></div>

                                    </c:if>
                                </c:forEach>
                                <div class="details_image_large"><img src="<c:url value="/resources/images/details_1.jpg"/>"> <div class="product_extra product_new"></div></div>

                                <div class="details_image_thumbnails d-flex flex-row align-items-start justify-content-between">
                                    <div class="details_image_thumbnail active" data-image="<c:url value="/resources/images/details_1.jpg"/>"><img src="<c:url value="/resources/images/details_1.jpg"/>" alt=""></div>
                                    <div class="details_image_thumbnail" data-image="<c:url value="/resources/images/details_2.jpg"/>"><img src="<c:url value="/resources/images/details_2.jpg"/>" alt=""></div>
                                    <div class="details_image_thumbnail" data-image="<c:url value="/resources/images/details_3.jpg"/>"><img src="<c:url value="/resources/images/details_3.jpg"/>" alt=""></div>
                                    <div class="details_image_thumbnail" data-image="<c:url value="/resources/images/details_4.jpg"/>"><img src="<c:url value="/resources/images/details_4.jpg"/>" alt=""></div>
                                </div>
                            </div>
                        </div>

                        <!-- Product Content -->
                        <div class="col-lg-6">
                            <div class="details_content">

                                <div class="details_name">${productDetail.name}</div>
                                <div class="details_price"><fmt:formatNumber value="${productDetail.price}"
                                                  pattern="###,###" type="number"/>VNĐ</div><br><br>
                                <div class="details_discount"><fmt:formatNumber value=""
                                                  pattern="###,###" type="number"/></div>


                                <!-- In Stock -->
                                <div class="in_stock_container">
                                    <div class="availability">Availability:</div>
                                    <span>${message}</span>
                                </div>
                                <div class="in_stock_container">
                                    <sec:authorize access="isAuthenticated()">
                                        <div class="product_extra product_new" style="transform: rotate(0deg);margin-left:-54px;"><a href="${pageContext.request.contextPath}/user/favorite/${productDetail.id}" class="like-btn"><span class="lnr lnr-heart"></span>Like</a></div>
                                    </sec:authorize>
                                </div>
                                <p>${error}</p>
                                <!-- Product Quantity --><c:if test="${message == null}">
                                    <form action="${pageContext.request.contextPath}/updateQuantity/${productDetail.id}" method="POST" class="form-inline">

                                        <div class="product_quantity_container">
                                            <div class="product_quantity clearfix">
                                                <span>Qty</span>
                                                <input  path="quantity_input" name="quantity_input" class="form-control" id="quantity_input" type="text" pattern="[0-9]*" value="1"/>
                                                <div class="quantity_buttons">
                                                    <div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                                                    <div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
                                                </div>
                                            </div>

                                            <input type="submit" value="Add to cart" class="button cart_button" />


                                    </form>

                                </div>
                            </c:if>

                            <!-- Share -->
                            <div class="details_share">
                                <span>Share:</span>
                                <ul>
                                    <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="row description_row">
                        <div class="col">
                            <div class="description_title_container">
                                <div class="description_title"><a href="${pageContext.request.contextPath}/description/${productDetail.id}">Description            |    </a></div>

                                <div class="reviews_title"><a href="${pageContext.request.contextPath}/technical/${productDetail.id}">Technical specifications            |</a></div>

                                <div class="reviews_title"><a href="#"><a href="${pageContext.request.contextPath}/review/${productDetail.id}">Reviews <span></span></a></div>
                            </div>
                            <c:if test="${message1 == 'description'}">
                                <div class="description_text">
                                    <p>${productDetail.description}</p>
                                </div>
                            </c:if>
                            <c:if test="${message1 == 'technical'}">
                                <div class="reviews_title">
                                    <Br><Br><Br><Br>
                                    <h3>Technical specifications</h3>
                                    <table class="table table-striped">

                                        <tr>
                                            <td>Screen:</td>
                                            <td>${productDetail.screen}</td>
                                        </tr>
                                        <tr>
                                            <td>Operating system:</td>
                                            <td>${productDetail.frontCamera}</td>
                                        </tr>
                                        <tr>
                                            <td>Rear camera:</td>
                                            <td>${productDetail.rearCamera}</td>
                                        </tr>
                                        <tr>
                                            <td>Front camera:</td>
                                            <td>${productDetail.frontCamera}</td>
                                        </tr>
                                        <tr>
                                            <td>RAM:</td>
                                            <td>${productDetail.ram}</td>
                                        </tr>
                                        <tr>
                                            <td>Memory card:</td>
                                            <td>${productDetail.memoryCard}</td>
                                        </tr>
                                        <tr>
                                            <td>Battery capacity:</td>
                                            <td>${productDetail.battery}</td>
                                        </tr>

                                    </table> 
                                </div>
                            </c:if>
                            <div class="reviews_title">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Products -->


            <!-- Newsletter -->

            <div class="newsletter">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="newsletter_border"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="newsletter_content text-center">
                                <div class="newsletter_title">Subscribe to our newsletter</div>
                                <div class="newsletter_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie eros</p></div>
                                <div class="newsletter_form_container">
                                    <form action="#" id="newsletter_form" class="newsletter_form">
                                        <input type="email" class="newsletter_input" required="required">
                                        <button class="newsletter_button trans_200"><span>Subscribe</span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <jsp:include page="include/js-page.jsp" />
        <script src="<c:url value="/resources/js/product.js"/>"></script>
    </body>
</html>
