<%-- 
    Document   : product
    Created on : Nov 27, 2020, 4:44:15 PM
    Author     : BTD
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Shoppy an Admin Panel Category Flat Bootstrap Responsive Website Template | Product :: w3layouts</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <jsp:include page="include-management/css-header-admin.jsp" />
        <jsp:include page="include-management/js-header-admin.jsp" />
    </head>
    <body>
        <div class="page-container">	
            <div class="left-content">
                <div class="mother-grid-inner">
                    <jsp:include page="include-management/header-body-admin.jsp" />
                    <jsp:include page="include-management/script-for sticky-nav.jsp"/>
                    <!--inner block start here-->
                    <div class="inner-block">
                        <div class="product-block">
                            <div class="pro-head">
                                <h2>Products</h2>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro1.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro1.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>256 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro2.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro2.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>156 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro3.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro3.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>500 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro4.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro4.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>188 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro5.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro5.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>220 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro6.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro6.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>160 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro7.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro7.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>350 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro8.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro8.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>500 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro9.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro9.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>256 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro10.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro10.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>548 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro3.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro3.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>390 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 product-grid">
                                <div class="product-items">
                                    <div class="project-eff">
                                        <div id="nivo-lightbox-demo"> <p> <a href="<c:url value="/resources-management/images/pro12.jpg"/>"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo"><span class="rollover1"> </span> </a></p></div>     
                                        <img class="img-responsive" src="<c:url value="/resources-management/images/pro12.jpg"/>" alt="">
                                    </div>
                                    <div class="produ-cost">
                                        <h4>Temporibus autem</h4>
                                        <h5>150 $</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <!--inner block end here-->
                    <jsp:include page="include-management/Copyright.jsp"/>
                </div>
            </div>
            <jsp:include page="include-management/menu-admin.jsp"/>
    </body>
</html>
